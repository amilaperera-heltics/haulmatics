import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { Routes, RouterModule } from '@angular/router';

const AdminRoutes: Routes = [
  { path: '/user', component: UserComponent },
  { path: '/userInfo', component: UserInfoComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UserInfoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      AdminRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
