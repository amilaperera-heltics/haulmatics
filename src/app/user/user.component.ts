import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpModule, Http } from '@angular/http';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userInfo={"firstName":String,"lastName":String};



  constructor(private router : Router,private httpClient:Http) { }

  addUser(){

    let userData={'orgId':"ANGULAR_"+this.userInfo.firstName+" "+this.userInfo.lastName};

    this.httpClient.post('https://api/example', userData)
    .subscribe(response => console.log(response));
  }

  ngOnInit() {
  }

}
